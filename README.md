# DALPHI study data repository

This repo contains the data sets used for and obtained from the study regarding [DALPHI](https://github.com/Dalphi/dalphi)'s pre-annotation assistance system ["The DALPHI annotation framework & how its pre-annotations can improve annotator efficiency" (Robert Greinacher and Franziska Horn, 2018)](https://arxiv.org/abs/1808.05558)

If any of this data was helpful for your research, please consider citing it:

    @article{greinacher2018dalphi,
      title     = {The DALPHI annotation framework \& how its pre-annotations can improve annotator efficiency},
      author    = {Greinacher, Robert and Horn, Franziska},
      journal   = {arXiv preprint arXiv:1808.05558},
      year      = {2018}
    }